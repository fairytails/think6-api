
think6-api 是在原作基础上进行`thinkphp6.*`进行适配制作，未修改任何原生方法，本意是为了自用。（原作长时间未更新）

与原作区别说明：
 - 改变命名空间
 - 调整容器获取request实例失效
 - 调整配置文件获取失效

特别说明：
- php 7.1.0+
- thinkphp 6.0+

使用方法参考原作：[think-api](https://github.com/czewail/think-api)

