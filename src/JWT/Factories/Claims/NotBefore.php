<?php
namespace Mumozi\Api\JWT\Factories\Claims;

class NotBefore extends Claim
{
    /**
     * Name
     */
    protected $name = 'nbf';
}
