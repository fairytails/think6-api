<?php
namespace Mumozi\Api\JWT\Factories\Claims;

class Issuer extends Claim
{
    /**
     * Name
     */
    protected $name = 'iss';
}
