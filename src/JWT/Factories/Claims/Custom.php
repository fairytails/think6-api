<?php 
namespace Mumozi\Api\JWT\Factories\Claims;

class Custom extends Claim
{
    protected $name;

    public function __construct($name, $value)
    {
        $this->name = $name;
        parent::__construct($value);
    }
}
