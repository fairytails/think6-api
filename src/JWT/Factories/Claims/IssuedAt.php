<?php
namespace Mumozi\Api\JWT\Factories\Claims;

class IssuedAt extends Claim
{
    /**
     * Name
     */
    protected $name = 'iat';
}
