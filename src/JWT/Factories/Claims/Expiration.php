<?php
namespace Mumozi\Api\JWT\Factories\Claims;

use Config;

class Expiration extends Claim
{
    /**
     * Name
     */
    protected $name = 'exp';

}
