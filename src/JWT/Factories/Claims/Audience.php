<?php
namespace Mumozi\Api\JWT\Factories\Claims;

class Audience extends Claim
{
    /**
     * Name
     */
    protected $name = 'aud';
}
