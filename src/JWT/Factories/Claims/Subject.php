<?php
namespace Mumozi\Api\JWT\Factories\Claims;

class Subject extends Claim
{
    /**
     * Name
     */
    protected $name = 'sub';
}
