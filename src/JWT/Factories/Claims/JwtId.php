<?php
namespace Mumozi\Api\JWT\Factories\Claims;

class JwtId extends Claim
{
    /**
     * Name
     */
    protected $name = 'jti';
}
