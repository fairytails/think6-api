<?php 
namespace Mumozi\Api\Exceptions;

class TokenExpiredException extends JWTException
{
}