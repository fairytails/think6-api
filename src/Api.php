<?php 
namespace Mumozi\Api;

use Mumozi\Api\Response\Factory as ResponseFactory;
use Mumozi\Api\JWT\Factory as JWTFactory;
use think\Config;

trait Api
{

    protected $response;
    protected $jwt;

    function __construct()
    {
        $this->init();
    }

    protected function init() {
        $this->response = new ResponseFactory;
        $this->jwt = new JWTFactory;
    }
}