<?php
namespace Mumozi\Api\Facades;

use think\Facade;

class JWT extends Facade
{
    protected static function getFacadeClass()
    {
        return 'Mumozi\Api\JWT\Factory';
    }
}