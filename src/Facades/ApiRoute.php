<?php
namespace Mumozi\Api\Facades;

use think\Facade;

class ApiRoute extends Facade
{
    protected static function getFacadeClass()
    {
        return 'Mumozi\Api\Routing\Router';
    }
}