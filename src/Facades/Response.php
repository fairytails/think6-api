<?php 
namespace Mumozi\Api\Facades;

use think\Facade;

class Response extends Facade
{
    protected static function getFacadeClass()
    {
        return 'Mumozi\Api\Response\Factory';
    }
}
