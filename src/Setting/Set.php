<?php 
namespace Mumozi\Api\Setting;

use think\facade\Config;

class Set
{
    protected static $files = [
        'resources' => 'resources.php',
        'api' => 'api.php',
        'jwt' => 'jwt.php',
    ];

    public static function __callStatic($func, $args)
    {
        $path = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR;
        if ($file = self::$files[$func]) {
            $config = require($path . $file);
            $_config = Config::get($func);
            if ($_config && is_array($_config)) {
                $config = array_merge($config, $_config);
            }
            call_user_func_array($args[0], [$config]);
        }
    }
}
